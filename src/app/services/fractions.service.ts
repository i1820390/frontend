import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FractionsService {
  url: string = "http://206.189.173.158:3000/api/fractions";

  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get(this.url);
  }
}
