import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class DistributionsService {


  url: string = "http://206.189.173.158:3000/api/distributions";

  constructor(
    private http: HttpClient
  ) { }

  findByTable(table_id: any, date_reservation: any) {
    return this.http.get(`${this.url}-by-table?table_id=${table_id}&date_reservation=${date_reservation}`);
  }

  findBySale(sale_id: number) {
    return this.http.get(`${this.url}-by-sale?sale_id=${sale_id}`);
  }
}
