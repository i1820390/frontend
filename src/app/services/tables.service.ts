import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class TablesService {
  url: string = "http://206.189.173.158:3000/api/table";

  constructor(
    private http: HttpClient
  ) { }

  create(nuevo: any) {
    return this.http.post(this.url, nuevo);
  }

  update(updated: any) {
    return this.http.put(this.url, updated);
  }

  findAll() {
    return this.http.get(`${this.url}s`);
  }

  uploadPhoto(formData: FormData) {
    return this.http.post(`${this.url}-upload-photo`, formData);
  }

  delete(id: number) {
    let body = {
      id: id
    }
    return this.http.delete(`${this.url}-delete?id=${id}`)
  }

}
