import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UsersService {

  url: string = "http://206.189.173.158:3000/api";

  constructor(
    private http: HttpClient
  ) { }

  login(username: string, password: string) {
    return this.http.post(`${this.url}/login`, {username: username, password: password} );
  }

  create(nuevo: any) {
    return this.http.post(`${this.url}/user`, nuevo);
  }

  forgotPassword(email: string) {
    let body = {
      email: email
    };
    return this.http.post(`${this.url}/forgot-password`, body);
  }

  changePassword(userId: number, newPassword: string) {
    let body = {
      userId: userId,
      newPassword: newPassword
    };
    return this.http.post(`${this.url}/change-password`, body);
  }

  getByParam(param: any) {
    return this.http.get(`${this.url}/user-by?param=${param}`);
  }

  findAllInexact() {
    return this.http.get(`${this.url}/users`);
  }

  delete(id: number) {
    let body = {
      id: id
    }
    return this.http.delete(`${this.url}/user-delete?id=${id}`)
  }

  put(updated: any) {
    return this.http.put(`${this.url}/user-update`, updated);
  }


}
