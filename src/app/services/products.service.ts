import { Injectable } from '@angular/core';
import { HttpClient, HttpClientModule } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ProductsService {

  url: string = "http://206.189.173.158:3000/api/products";

  constructor(
    private http: HttpClient
  ) { }

  findAll() {
    return this.http.get(this.url);
  }

  create(nuevo: any) {
    return this.http.post(this.url, nuevo);
  }

  uploadPhoto(formData: FormData) {
    return this.http.post(`${this.url}-upload-photo`, formData);
  }

  delete(id: number) {
    let body = {
      id: id
    }
    return this.http.delete(`${this.url}-delete?id=${id}`)
  }

  put(updated: any) {
    return this.http.put(`${this.url}-update`, updated);
  }


}
