import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class SaleDetailsService {

  
  url: string = "http://206.189.173.158:3000/api/sale-detail";

  constructor(
    private http: HttpClient
  ) { }

  create(nuevo: any) {
    return this.http.post(this.url, nuevo);
  }

  findBySaleId(sale_id: number) {
    return this.http.get(`${this.url}-by-sale?sale_id=${sale_id}`);
  }

}
