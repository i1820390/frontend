import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoriesService {

  url: string = "http://206.189.173.158:3000/api/categories";

  constructor(
    private http: HttpClient
  ) { }

  create(nuevo: any) {
    return this.http.post(this.url, nuevo);
  }

  findAll() {
    return this.http.get(this.url);
  }

}
