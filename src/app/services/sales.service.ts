import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class SalesService {

  url: string = "http://206.189.173.158:3000/api/sale";

  constructor(
    private http: HttpClient
  ) { }

  create(nuevo: any, detalle: any) {
    let body = {
      cabecera: nuevo,
      detalle: detalle
    }
    return this.http.post(this.url, body);
  }

  findAll(customer_id: number = 0) {
    return this.http.get(`${this.url}s${customer_id !== 0 ? '?customer_id='+customer_id: ''}`);
  }

  update(sale: any, detail: any) {
    let body = {
      cabecera: sale,
      detalle: detail
    }
    return this.http.put(this.url, body);
  }

  delete(sale_id: number) {
    return this.http.delete(`${this.url}?sale_id=${sale_id}`,)
  }

  findAllReport() {
    return this.http.get(`${this.url}s-report`);
  }


}
