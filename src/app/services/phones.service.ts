import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PhonesService {

  url: string = "http://206.189.173.158:3000/api/phones";

  constructor(
    private http: HttpClient
  ) { }

  findByUser(user_id: number) {
    return this.http.get(`${this.url}?user_id=${user_id}`);
  }
}
