import { TestBed } from '@angular/core/testing';

import { FractionsService } from './fractions.service';

describe('FractionsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FractionsService = TestBed.get(FractionsService);
    expect(service).toBeTruthy();
  });
});
