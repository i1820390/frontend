import { Component, OnInit, TemplateRef, ViewChild, HostListener } from '@angular/core';
import { Router } from '@angular/router';
import { SalesService } from '../services/sales.service';
import { SaleDetailsService } from '../services/sale-details.service';
import { BsModalRef, BsModalService, BsDatepickerDirective, ModalDirective } from 'ngx-bootstrap';
import { TablesService } from '../services/tables.service';
import * as Moment from 'moment';
import { FractionsService } from '../services/fractions.service';
import { DistributionsService } from '../services/distributions.service';
import { ProductsService } from '../services/products.service';
import { CategoriesService } from '../services/categories.service';


@Component({
  selector: 'app-reservation',
  templateUrl: './reservation.component.html',
  styleUrls: ['./reservation.component.css']
})
export class ReservationComponent implements OnInit {

  @ViewChild(BsDatepickerDirective, { static: false }) datepicker: BsDatepickerDirective;
  @ViewChild('newReservationModal', {static: true}) newReservationModal: ModalDirective;
  @ViewChild('confirmationModal', {static: true}) confirmationModal: ModalDirective;

  @ViewChild('detailModal', {static: true}) detailModal: ModalDirective;

  bsInlineValue = new Date();
  modalRef: BsModalRef;
  diner: number = 1;
  userLoged: any;
  currentDate: any = Moment().add(1,'days').toDate();
  constructor(
    private ModalService: BsModalService,
    private Router: Router,
    private Sale: SalesService,
    private SaleDetail: SaleDetailsService,
    private Table: TablesService,
    private Fraction: FractionsService,
    private Product: ProductsService,
    private Distribution: DistributionsService,
    private Category: CategoriesService
  ) { 
    this.userLoged = JSON.parse(localStorage.getItem('userReservation'));
    
    if (!this.userLoged) {
      this.Router.navigateByUrl('welcome');
    }
    this.getReservations(this.userLoged.id);
    this.getTables();
    this.getFractions();
    this.getProducts();
    
  }

  categories: any[] = [];

  getCategories() {
    this.Category.findAll().subscribe((data: any) => {
      this.categories = data;
      if (this.categories.length) {
        this.categories[0].selected = true;
        this.getCategorySelected();
      }
    });
  }

  categorySelected: any;

  selectCategory(c: any) {
    this.categories.map(i => i.selected = false);
    c.selected = true;
    this.getCategorySelected();
  }

  productsFiltereds:any[] = [];

  getCategorySelected() {
    this.categorySelected = this.categories.find(i => i.selected);
    this.productsFiltereds = this.products.filter(i => i.category_id === this.categorySelected.id);
  }


  getProducts() {
    this.Product.findAll().subscribe((data: any) => {
      this.products = data;
      this.getCategories();
    });
  }

  products: any[] = [];

  sale: any = {
    persons_quantity: 1
  };
  detail: any[] = [];
  total: number = 0;
  menus: any[] = [
    {
      id: 1,
      name: 'estofado',
      image: 'chaufa3.jpg',
      quantity: 0,
      price: 15.75
    },
    {
      id: 2,
      name: 'aji de gallina',
      image: 'chaufa4.jpeg',
      quantity: 0,
      price: 16
    },
    {
      id: 3,
      name: 'papa la huancaina',
      image: 'chaufa5.jpg',
      quantity: 0,
      price: 18
    },
    {
      id: 4,
      name: 'arroz con pollo',
      image: 'chaufa4.jpeg',
      quantity: 0,
      price: 25
    },
    {
      id: 5,
      name: 'escabeche',
      image: 'chaufa5.jpg',
      quantity: 0,
      price: 10
    }
  ];
  
  tables: any[] = [];
  tableSelected: any = {};
  reservations: any[] = [];
  fractions: any[] = [];
  qrCodeConfirmation: any;

  getReservations(customer_id) {
    this.Sale.findAll(customer_id).subscribe((data: any) => {
      this.reservations = data;
    });
  }

  getTables() {
    this.Table.findAll().subscribe((data: any) => {
      console.log(data);
      this.tables = data;
    });
  }

  getFractions() {
    this.Fraction.findAll().subscribe((data: any) => {
      this.fractions = data;
    });
  }


  selectDish(menu: any) {
    let existing = this.detail.find(i => i.id === menu.id);
    if (existing) {
      existing.quantity += 1;
    } else {
      menu.quantity += 1;
      this.detail.push(menu);
    }
  }

  selectTable(table: any) {
    this.tableSelected = table;
    this.sale.table_id = table.id;
    this.getDistributions();
  }

  getDistributions() {
    if (this.sale.table_id && this.sale.date_reservation) {
      console.log(typeof(this.sale.date_reservation));
      let date_reservation = Moment(this.sale.date_reservation).format('DD/MM/YYYY');
      console.log(date_reservation);
      this.Distribution.findByTable(this.sale.table_id, date_reservation).subscribe((data: any) => {
        this.tableSelected.distributions = data;
        this.fractions.map(i => {
          let fractionFound = this.tableSelected.distributions.find(y => y.fraction_id === i.id); 
          if (fractionFound) {
            i.bussy = true
          } else {
            i.bussy = false;
          }
        });
        console.log(this.fractions);
      });
    }
  }

  initFraction: any;
  finishFraction: any;
  selectFraction(f: any) {
    if (!this.sale.id) {
      if (!f.bussy) {
        if (!this.initFraction) {
          f.selected = true;
          this.initFraction = JSON.parse(JSON.stringify(f));
        } else {
          if (this.finishFraction) {
            this.fractions.map(i => i.selected = false);
          } 
          console.log(f);
          this.finishFraction = JSON.parse(JSON.stringify(f));
          if (this.initFraction.id > this.finishFraction.id) {
            this.fractions.filter(i => i.id <= this.initFraction.id && i.id >= this.finishFraction.id).map(x => x.selected = true);
            if (this.fractions.filter(i => i.selected).length > 2) {
              console.log('mas de una hora');
              alert('debes seleccionar solo un intervalo de una (01) horas.')
              this.restartFractions();
            }
          } else {
            this.fractions.filter(i => i.id <= this.finishFraction.id && i.id >= this.initFraction.id).map(x => x.selected = true);
            if (this.fractions.filter(i => i.selected).length > 2) {
              console.log('mas de una hora - al revez');
              alert('debes seleccionar solo un intervalo de una (01) horas.')
              this.restartFractions();
            }
          }
          
        }
      } else {
        alert('Esta hora esta reservada.');
        return false;
      }
    }
  }

  restartFractions() {
    this.fractions.map(i => i.selected = false);
    let initFraction = this.fractions.find(i => i.id === this.initFraction.id).selected = true;
    this.finishFraction = undefined;
  }

  cleanSelection() {
    this.initFraction = undefined;
    this.finishFraction = undefined;
    this.fractions.map(i => i.selected = false);
  }


  dismissDish(menu: any) {
    let existing = this.detail.find(i => i.id === menu.id);
    existing.quantity -= 1;
    if (existing.quantity === 0) {
      let index = this.detail.indexOf(existing);
      this.detail.splice(index, 1);
    }
  }

  getSubTotal(a: any) {
    a.total = a.quantity*a.price;
    return a.total;
  }

  getTotal(detail: any[]) {
    this.total = detail.reduce((memo, item) => { return memo + item.total},0);
    this.sale.total_amount = this.total;
    return this.total;
  }
  
  // removeItem(index: number, item: any) {
  //   item.quantity = 0;
  //   this.detail.splice(index, 1);
  // }

  // existsInDetail(m: any) {
  //   return this.detail.find(i => i.id === m.id) !== undefined;
  // }

  logout() {
    this.Router.navigateByUrl('/logout'); 
  }

  save(template: TemplateRef<any>) {
    if (this.sale.id) {
      // edite
      this.Sale.update(this.sale, this.detail).subscribe((data: any) => {
        this.qrCodeConfirmation = data.qrcode;
        // this.openModal(template);
      });
    } else {
      // guardar
      console.log(this.sale);
      //this.sale.table_id = 1;
      this.sale.user_id = 0;
      this.sale.customer_id = this.userLoged.id;
      this.sale.customer_email = this.userLoged.email;
      this.sale.table_name = this.tableSelected.name;
      console.log(this.detail);
      this.Sale.create(this.sale, this.detail).subscribe((data: any) => {
        console.log(data)
        console.log('mostrar QR');
        this.qrCodeConfirmation = data.qrcode;
        // this.openModal(template);
      });
    }
  }

  openModal(modal: string) {
    switch (modal) {
      case 'newReservation':
        this.restartVars();
        this.newReservationModal.config.ignoreBackdropClick = true;
        this.getDetailSelected();
        this.newReservationModal.show();
        break;
    
      default:
        break;
    }
  }

  closeModal(modal: string) {
    switch (modal) {
      case 'newReservation':
        this.newReservationModal.hide();
        this.restartVars();
        break;
      case 'confirmation':
        this.confirmationModal.hide();
        break;
      default:
        break;
    }
  }

  restartVars() {
    this.sale = {
      persons_quantity: 1
    };
    this.saleDetails = [];
    this.tableSelected = {};
    this.initFraction = undefined;
    this.finishFraction = undefined;
    this.details.map(i => i.selected = false);
    this.details[0].selected = true;
  }


  // openModal(template: TemplateRef<any>) {
  //   this.ModalService.config.ignoreBackdropClick = true;
  //   this.modalRef = this.ModalService.show(template);
  // }

  // closeModal() {
  //   this.sale = {
  //     persons_quantity: 1
  //   };
  //   this.detail = [];
  //   this.menus.map(i => i.quantity = 0);
  //   this.tableSelected = {};
  //   this.modalRef.hide();
  // }


  // editSale(reservation: any) {
  //   this.sale = JSON.parse(JSON.stringify(reservation));
  //   let tableSelected = this.tables.find(i => i.id === this.sale.table_id);
  //   this.selectTable(tableSelected);
  //   this.getDetailOfSale(this.sale.id);
  // }

  editSale(r: any) {
    r.politics = true;
    this.sale = JSON.parse(JSON.stringify(r));
    let tableSelected = this.tables.find(i => i.id === this.sale.table_id);
    this.tableSelected.politics = true;
    this.newReservationModal.show();
    this.newReservationModal.config.ignoreBackdropClick = true;
    this.getDetailSelected();
    this.selectTable(tableSelected);
    this.getDetails(this.sale.id);
    this.getDistributionsBySale(this.sale.id);
  }

  getDetails(id: number) {
    this.SaleDetail.findBySaleId(id).subscribe((data: any) => {
      this.saleDetails = data;
      this.saleDetails.map(i => {
        let productFound = this.products.find(x => x.id === i.product_id);
        if (productFound) {
          i.photo = productFound.photo;
        }
      });
    });
  }

  getDistributionsBySale(sale_id: number) {
    this.Distribution.findBySale(sale_id).subscribe((data: any) => {
      let idsFractions = data.map(i => i.fraction_id);
      console.log(idsFractions);
      this.fractions.map(i => {
        i.bussy = idsFractions.includes(i.id);
      });
    })
  }

  getDetailOfSale(id: number) {
    this.SaleDetail.findBySaleId(id).subscribe((data: any) => {
      console.log(data);
      this.detail = data;
      this.detail.map(i => {
        let dishFound = this.menus.find(y => y.id === i.product_id);
        if (dishFound) {
          i.name = dishFound.name;
          i.image = dishFound.image;
        };
      });
    })
  }

  togglePerson(type: string) {
    switch (type) {
      case 'minus':
        if (this.sale.persons_quantity>1) {
          this.sale.persons_quantity -= 1;
        }
        break;
      case 'plus':
        if (this.sale.persons_quantity !== 8) {
          this.sale.persons_quantity +=1;
        }
      default:
        break;
    }
  }

  // Tab de Detalles
  details: any[] = [
    {
      id: 'politics',
      name: 'politicas',
      selected: true
    },
    {
      id: 'reservation',
      name: 'de la reserva',
      selected: false
    },
    {
      id: 'delivery',
      name: 'del pedido',
      selected: false
    }
  ];

  detailSelected: any;
  selectDetail(d: any) {
    if (this.sale.politics === true) {
      this.details.map(i => i.selected = false);
      d.selected = true;
      this.getDetailSelected();
    } else {
      alert('Necesitas aceptar las condiciones para continuar');
    }
  }

  getDetailSelected() {
    this.detailSelected = this.details.find(i => i.selected);
  }

  saleDetails:any[] = [];

  existsInDetail(p: any) {
    return this.saleDetails.filter(i => i.product_id === p.id).length > 0;
  }

  getProductName(d: any) {
    let productFound = this.products.find(i => i.id === d.product_id);
    if (productFound) {
      d.name = productFound.name;
      return productFound.name;
    } else {
      return 'producto no encontrado'
    }
  }

  addItem(p) {
    if (this.existsInDetail(p)) {
      let productFound = this.saleDetails.find(i => i.product_id === p.id);
      if (productFound) {
        productFound.quantity +=1;
        this.getTotals();
      }
    } else {
      this.saleDetails.push({
        product_id: p.id,
        name: p.name,
        quantity: 1,
        photo: p.photo,
        unit_price: p.unit_price,
      });
      this.getTotals();
    }
  }

  removeItem(p) {
    let productFound = this.saleDetails.find(i => i.product_id === p.id);
    if (productFound) {
      if (productFound.quantity === 1) {
        this.saleDetails.splice(this.saleDetails.indexOf(productFound), 1);
        this.getTotalAmount()
      } else {
        productFound.quantity -= 1;
        this.getTotals();
      }
    }
  }

  deleteItem(i) {
    this.saleDetails.splice(i, 1);
    this.getTotalAmount();
  }

  getTotals() {
    this.saleDetails.map(i => {
      i.price = i.unit_price * i.quantity;
    });
    this.getTotalAmount()
  }

  getTotalAmount() {
    this.sale.total_amount = this.saleDetails.reduce((memo, i) => { return memo + i.price}, 0);
  }

  ngOnInit() {
  }

  saveSale() {
    console.log(this.fractions.filter(i => i.selected));
    this.sale.user_id = 0;
    this.sale.fractions = this.fractions.filter(i => i.selected);
    this.sale.customer_id = this.userLoged.id;
    this.sale.customer_email = this.userLoged.email;
    this.sale.table_name = this.tableSelected.name;
    this.sale.initFraction = this.initFraction;
    this.sale.finishFraction = this.finishFraction;
    this.Sale.create(this.sale, this.saleDetails).subscribe((data: any) => {
      console.log(data)
      console.log('mostrar QR');
      this.sale.create_at = new Date();
      this.qrCodeConfirmation = data.qrcode;
      this.newReservationModal.hide();
      this.getReservations(this.userLoged.id);
      this.confirmationModal.show();
      this.confirmationModal.config.ignoreBackdropClick = true;
    });
  }

  SaveEditSale() {
    this.sale.customer_id = this.userLoged.id;
    this.sale.customer_email = this.userLoged.email;
    this.sale.table_name = this.tableSelected.name;
    this.Sale.update(this.sale, this.saleDetails).subscribe((data: any) => {
      this.qrCodeConfirmation = data.qrcode;
      this.newReservationModal.hide();
      this.getReservations(this.userLoged.id);
      this.confirmationModal.show();
      this.confirmationModal.config.ignoreBackdropClick = true;
    });
  }

  goTo(tabName: string) {
    let detailDelivery = this.details.find( i => i.id === tabName);
    if (detailDelivery) {
      console.log(detailDelivery);
      this.selectDetail(detailDelivery);
    }
  }

  deleteReservation(r: any) {
    console.log(r);
    this.Sale.delete(r.id).subscribe((data: any) => {
      alert('Se elimino la reserva');
      this.getReservations(this.userLoged.id);
    });
  }
 


}

