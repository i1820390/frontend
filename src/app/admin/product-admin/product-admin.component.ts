import { Component, OnInit,ElementRef, ViewChild } from '@angular/core';
import { CategoriesService } from '../../services/categories.service';
import { ProductsService } from '../../services/products.service';
import { SaleDetailsService } from 'src/app/services/sale-details.service';
import { SalesService } from 'src/app/services/sales.service';
import { ModalDirective } from 'ngx-bootstrap';


@Component({
  selector: 'app-product-admin',
  templateUrl: './product-admin.component.html',
  styleUrls: ['./product-admin.component.css']
})
export class ProductAdminComponent implements OnInit {


  newProduct: any = {}; 
  categories: any = [];
  products: any = [];
  
  

  editingProduct: boolean = false;

  constructor(
    private Categories: CategoriesService,
    private Products: ProductsService,
    private el: ElementRef,
  
  ) {
    
    this.getCategories();
    this.getProducts();
    
   }



   getProducts() {
    this.Products.findAll().subscribe((data: any) => {
      this.products = data; 
    });
  }

  getCategories() {
    this.Categories.findAll().subscribe((data: any) => {
      this.categories = data;
    });
  }
  
  saveProduct() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('photo', inputEl.files.item(0));
      this.Products.uploadPhoto(formData).subscribe((data: any) => {
        this.newProduct.photo = data.file.filename;
        this.Products.create(this.newProduct).subscribe((productCreated: any) => {
          this.newProduct = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getProducts();
          alert('Se registro el producto satisfactoriamente.');
        });
      });
    }
  }

  deleteModalData: any = {}

  removeProduct(id: number, index: number) {
    let result = confirm(`¿Estas seguro que deseas eliminar el Producto ${this.getProductName(id).toUpperCase()}?`);
    if (result) {
      this.Products.delete(id).subscribe((data: any) => {
        this.products.splice(index, 1);
      });
    } else {
      return false;
    }
  }
  

  getProductName(id: number) {
    let productFound = this.products.find(i => i.id === id);
    if (productFound) {
      return productFound.name;
    } else {
      return 'producto no encontrado';
    }
  }

  editProduct(p: any) {
    this.newProduct = JSON.parse(JSON.stringify(p));
    this.editingProduct = true;
  }
  
  cancelEditProduct() {
    this.newProduct = {};
    this.editingProduct = false;
  
  }

  saveEditProduct() {
    if (this.editingProduct && !this.newProduct.photo) {
      let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
      let fileCount: number = inputEl.files.length;
      let formData = new FormData();
      if (fileCount > 0) { // a file was selected
        formData.append('photo', inputEl.files.item(0));
        this.Products.uploadPhoto(formData).subscribe((data: any) => {
          this.editingProduct = false;
          this.newProduct.photo = data.file.filename;
          this.Products.put(this.newProduct).subscribe((productCreated: any) => {
            this.newProduct = {};
            inputEl.files = undefined;
            inputEl.value = '';
            this.getProducts();
            alert('Se actualizo el producto satisfactoriamente.');
          });
        });
      }
    } else {
      this.Products.put(this.newProduct).subscribe((productCreated: any) => {
        this.newProduct = {};
        // inputEl.files = undefined;
        // inputEl.value = '';
        this.getProducts();
        alert('Se actualizo el producto satisfactoriamente.');
      });
    }
    
  }

  deletePhoto() {
    this.newProduct.photo = undefined;
  }


  ngOnInit() {
  }

}

