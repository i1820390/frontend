import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {
  userLoged: any
  constructor(
    private Router: Router,
  ) { 
    this.userLoged = JSON.parse(localStorage.getItem('userReservation'));
    console.log(this.userLoged);
  }

  logout() {
    this.Router.navigateByUrl('logout');
  }

  ngOnInit() {
  }

}
