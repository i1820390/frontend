import { Component, OnInit } from '@angular/core';
import { UsersService } from '../../services/users.service';
import { Router } from '@angular/router';
import { PhonesService } from '../../services/phones.service';

@Component({
  selector: 'app-user-admin',
  templateUrl: './user-admin.component.html',
  styleUrls: ['./user-admin.component.css']
})
export class UserAdminComponent implements OnInit {

  newUser: any = {};
  users: any = [];

  editingUser: boolean = false;

  constructor(
    private Router: Router,
    private User: UsersService,
    private Phones: PhonesService,
  ) {
    this.getUsers();
   }

   logout() {
    this.Router.navigateByUrl('logout');
  }

   getUsers() {
    this.User.findAllInexact().subscribe((data: any) => {
      this.users = data;
    });
  }

  getPhones() {
    this.users.map(i => {
      i.password = undefined;
      this.Phones.findByUser(i.id).subscribe((data: any) => {
        if (data.length) {
          i.phone = data[0].phone_number;
        } else {
          i.phone = undefined;
        }
      });
    })
  }
  genders: any[] = [
    {
      id: 'male',
      title: 'masculino'
    },
    {
      id: 'female',
      title: 'femenino'
    }
  ];

  userTypes: any[] = [
    {
      id: 'fa fa-cog',
      title: 'Administrador',
      name: 'admin',
      selected: false
    },
    {
      id: 'fa fa-user',
      title: 'Usuario',
      name: 'user',
      selected: false
    },
    {
      id: 'fa fa-money',
      title: 'Cliente',
      name: 'customer',
      selected: false
    }
  ];
  searching: boolean = false;
  error_email: boolean = false;
  error_username: boolean = false;
  email_exist: boolean = true;


  selectGender(gender) {
    this.newUser.gender = gender;
  }

  invalidForm(f: any) {
    return f.invalid || !this.newUser.gender || this.error_email || this.error_username;
  }

  validateField(field: string) {
    if (this.newUser[field]) {
      this.searching = true;
      this.email_exist = true;
      this.User.getByParam(this.newUser[field]).subscribe((data: any) => {
        console.log(data);
        if (field === 'username') {
          this.error_username = data.length > 0;
        } else {
          this.error_email = data.result.length > 0;
          this.email_exist = JSON.parse(data.verifier.smtpCheck);
          this.searching = false;
        }
      });
    }
  }
  selectUserType(u: any) {
    this.userTypes.map(i => i.selected = false);
    u.selected = true;
    this.newUser.user_type = u.name;
  }
  getIcon(u: any) {
    let userFound = this.userTypes.find(i => i.name === u.user_type);
    if (userFound) {
      return userFound.id;
    } else {
      'NO SE REGISTRO TIPO';
    }
  }

  removeUser(id: number, index: number) {
    this.User.delete(id).subscribe((data: any) => {
      this.users.splice(index, 1);
    });
  }
  cancelEditUser() {
    this.newUser = {
      gender: true
    };
    this.editingUser = false;
    this.userTypes.map(i => {
      i.selected = false;
    });
  }

  saveEditUser() {
    console.log(this.newUser);
    let body = {
      newUser: this.newUser
    };
    if (this.newUser.password) {
      this.User.changePassword(this.newUser.id, this.newUser.password).subscribe((data: any) => {
        console.log(data);
        console.log('Se cambio la contraseña');
      });
    }
    this.User.put(body).subscribe((data: any) => {
      console.log(data);
      this.newUser = {
        gender: true
      };
      this.editingUser = false;
      this.userTypes.map(i => {
        i.selected = false;
      });
      alert('Usuario Actualizado');
      this.getUsers();
    });
  }
  
  editUser(u: any) {
    this.newUser = JSON.parse(JSON.stringify(u));
    this.editingUser = true;
    this.userTypes.map(i => {
      i.selected = false;
    });
    let userTypeSelected = this.userTypes.find(i => i.name === this.newUser.user_type);
    if (userTypeSelected) {
      userTypeSelected.selected = true;
    }
  }
  
  ngOnInit() {
  }

}
