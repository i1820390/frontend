import { Component, OnInit,ElementRef } from '@angular/core';
import { TablesService } from '../../services/tables.service';


@Component({
  selector: 'app-table-admin',
  templateUrl: './table-admin.component.html',
  styleUrls: ['./table-admin.component.css']
})
export class TableAdminComponent implements OnInit {

  newTable: any = {};
  tables: any = [];

  editingTable: boolean = false;

  constructor(
    private Tables: TablesService,
    private el: ElementRef,

    

  ) {
    this.getTables();
  }

  getTables() {
    this.Tables.findAll().subscribe((data: any) => {
      this.tables = data;
    });
  }
  
  saveTable() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#tablePhoto');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('tablePhoto', inputEl.files.item(0));
      this.Tables.uploadPhoto(formData).subscribe((data: any) => {
        console.log(data);
        this.newTable.photo = data.file.filename;
        let body = {
          jsonbulk: [this.newTable]
        };
        this.Tables.create(body).subscribe((productCreated: any) => {
          this.newTable = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getTables();
          alert('Se registro la mesa satisfactoriamente.');
        });
      });
    }
  }

  removeTable(id: number, index: number) {
    let result = confirm(`¿Estas seguro que deseas eliminar la mesa ${this.geTableName(id).toUpperCase()}?`);
    if (result) {
      this.Tables.delete(id).subscribe((data: any) => {
        this.tables.splice(index, 1);
      });
    } else {
      return false
    }
  }


  geTableName(id: number) {
    let tableFound = this.tables.find(i => i.id === id);
    if (tableFound) {
      return tableFound.name;
    } else {
      return 'producto no encontrado';
    }
  }

  editTable(table: any) {
    this.newTable = JSON.parse(JSON.stringify(table));
    this.editingTable = true;
  }

  cancelEditTable() {
    this.newTable = {};
    this.editingTable = false;
  }

  saveEditTable() {
    console.log(this.newTable);
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#tablePhoto');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('tablePhoto', inputEl.files.item(0));
      this.Tables.uploadPhoto(formData).subscribe((data: any) => {
        this.editingTable = false;
        console.log(data);
        this.newTable.photo = data.file.filename;
        let body = {
          jsonbulk: [this.newTable]
        };
        this.Tables.update(body).subscribe((productUpdated: any) => {
          console.log(productUpdated);
          this.newTable = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getTables();
          alert('Se actualizo la mesa satisfactoriamente.');
        });
      });
    }
  }
  deletePhoto() {
    this.newTable.photo = undefined;
  }
  ngOnInit() {
  }

}
