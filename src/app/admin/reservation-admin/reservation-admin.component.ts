import { Component, OnInit,ViewChild, ElementRef } from '@angular/core';
import { ModalDirective } from 'ngx-bootstrap';
import { SalesService } from 'src/app/services/sales.service';

import * as jspdf from 'jspdf';  
  
import html2canvas from 'html2canvas';
import { SaleDetailsService } from 'src/app/services/sale-details.service';
import { ProductsService } from 'src/app/services/products.service';

@Component({
  selector: 'app-reservation-admin',
  templateUrl: './reservation-admin.component.html',
  styleUrls: ['./reservation-admin.component.css']
})
export class ReservationAdminComponent implements OnInit {
   @ViewChild('newSaleModal', {static: true}) newSaleModal: ModalDirective;
   @ViewChild('detailModal', {static: true}) detailModal: ModalDirective;

   sales: any = [];

  constructor(
    private Sale: SalesService,
    private el: ElementRef,
    private SaleDetail: SaleDetailsService,
    private Product: ProductsService


  ) { 
    this.getSales();
    this.getProducts();
  }
products: any[] = [];

  getProducts(){
    this.Product.findAll().subscribe((data: any)=>{
    this.products=data;
    });
  }

getProductName(id: number){
  let productFound = this.products.find(i => i.id===id);
  if (productFound) {
    return productFound.name;
  } else {
    return "Producto no encontrado";
  }
}

  public captureScreen()  
  {  
    var data = document.getElementById('contentToConvert');
    console.log(data);
    setTimeout(() => {
      html2canvas(data).then(canvas => {  
        // Few necessary setting options  
        var imgWidth = 208;   
        var pageHeight = 295;    
        var imgHeight = canvas.height * imgWidth / canvas.width;  
        var heightLeft = imgHeight;  
    
        const contentDataURL = canvas.toDataURL('image/png')  
        let pdf = new jspdf('p', 'mm', 'a4'); // A4 size page of PDF  
        var position = 0;  
        pdf.addImage(contentDataURL, 'PNG', 0, position, imgWidth, imgHeight)  
        pdf.save('MYPdf.pdf'); // Generated PDF   
      });  
    }, 1500);  
  }

  link: string;
  total_sales = 0;
  getSales(){
    this.Sale.findAllReport().subscribe((data: any) => {
      this.sales = data.data;
      this.total_sales = this.sales.reduce((memo, i) => { return memo = memo + parseFloat(i.total_amount); }, 0);
      this.link = data.file.filename;
      console.log(data);

    });
 }

  reservations: any[] = [
    {
      id: 1,
      table_id: 1,
      products: [1,2,3],
      status: 'R',
      total_amount: Math.random()*30
    },
    {
      id: 2,
      table_id: 2,
      products: [1],
      status: 'A',
      total_amount: Math.random()*30
    },
    {
      id: 3,
      table_id: 3,
      products: [1,3],
      status: 'X',
      total_amount: Math.random()*30
    },
    {
      id: 4,
      table_id: 4,
      products: [2,3],
      status: 'R',
      total_amount: Math.random()*30
    }
  ];

  addSale() {
    this.newSaleModal.config.ignoreBackdropClick = true;
    this.newSaleModal.show();
  }

  ngOnInit() {
  }


  saleDetailed: any = {};

  detailReservation(s: any){
    console.log(s);
    this.SaleDetail.findBySaleId(s.sale_id).subscribe((data:any)=>{
      console.log(data);
      this.detailModal.show();
      s.detail= data;
      this.saleDetailed=s;
    });
  }


}
