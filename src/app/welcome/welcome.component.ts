import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Router } from '@angular/router';
import { UsersService } from '../services/users.service';

import { AuthService } from "angularx-social-login";
import { GoogleLoginProvider, SocialUser } from "angularx-social-login";

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {
  userLoged: any = {};
  modalRef: BsModalRef;
  constructor(
    private modalService: BsModalService,
    private Router: Router,
    private User: UsersService,
    private authService: AuthService
  ) { 
    this.getButtonSelected();
  }

  error_email: boolean = false;
  error_username: boolean = false;
  email_exist: boolean = true;
  slides: any[] = [
    {
      title: 'Bienvenido a ',
      image: 'slide1.jpg',
      description: 'Chifa Chau Fan',
      alt: 'first slide'
    },
    {
      title: 'Bienvenido a ',
      image: 'slide2.jpg',
      description: 'Chifa Chau Fan',
      alt: 'second slide'
    },
    {
      title: 'Bienvenido a ',
      image: 'slide3.jpg',
      description: 'Chifa Chau Fan',
      alt: 'third slide'
    },
  ];

  menus: any[] = [
    {
      id: 1,
      name: 'estofado',
      image: 'chaufa3.jpg',
      price: 15.75
    },
    {
      id: 2,
      name: 'aji de gallina',
      image: 'chaufa4.jpeg',
      price: 15.75
    },
    {
      id: 3,
      name: 'papa la huancaina',
      image: 'chaufa5.jpg',
      price: 15.75
    },
    {
      id: 4,
      name: 'arroz con pollo',
      image: 'chaufa4.jpeg',
      price: 15.75
    },
    {
      id: 5,
      name: 'escabeche',
      image: 'chaufa5.jpg',
      price: 15.75
    }
  ];

  buttons: any[] = [
    {
      id: 'login',
      title: 'Iniciar Sesión',
      active: true
    },
    {
      id: 'singup',
      title: 'Registrarse',
      active: false
    }
  ];

  genders: any[] = [
    {
      id: 'male',
      title: 'masculino'
    },
    {
      id: 'female',
      title: 'femenino'
    }
  ]

  buttonSelected: any;

  username: string;
  password: string;
  logining: boolean = false;
  error: any;
  forgotPassword: any;
  newUser: any = {
    gender: true
  };
  fieldToValidate: string;
  searching: boolean = false;

  getButtonSelected() {
    this.buttonSelected = this.buttons.find(i => i.active);
  }

  selectButton(button: any) {
    this.buttons.map(i => i.active = false);
    button.active = true;
    this.newUser = {
      gender: true
    };
    this.error_email = false;
    this.error_username = false;
    this.getButtonSelected();
  }

  openModal(template: TemplateRef<any>) {
    this.modalService.config.ignoreBackdropClick = true;
    this.modalRef = this.modalService.show(template);
  }

  closeModal() {
    this.newUser = {};
    this.username = undefined;
    this.password = undefined;
    this.buttons.map(i => i.active = false);
    this.buttons[0].active = true;
    this.getButtonSelected();
    this.modalRef.hide()
  }

  login() {
    this.logining = true;
    this.User.login(this.username, this.password).subscribe((data: any) => {
      console.log(data);
      this.logining = false;
      if (data) {
        data.isAdmin = this.isAdmin();
        localStorage.setItem('userReservation', JSON.stringify(data));
        if (this.isAdmin()) {
          this.modalRef.hide();
          this.Router.navigateByUrl('admin');
        } else {
          this.logining = false;
          this.modalRef.hide();
          
          this.Router.navigateByUrl('reservation');
        }
      } else {
        this.error = {
          message: 'No existe el usuario. Verifique sus datos!!!'
        }
        this.counter();
      }
    });
  }

  private user: SocialUser;
  private loggedIn: boolean;

  loginWithGoogle() {
    console.log('aqui');
    this.authService.signIn(GoogleLoginProvider.PROVIDER_ID).then(a => {
      console.log(a);
      this.User.login(a.email, null).subscribe((data: any) => {
        if (data) {
          data.isAdmin = this.isAdmin();
          localStorage.setItem('userReservation', JSON.stringify(data));
          if (this.isAdmin()) {
            this.modalRef.hide();
            this.Router.navigateByUrl('admin');
          } else {
            this.logining = false;
            this.modalRef.hide();
            
            this.Router.navigateByUrl('reservation');
          }
        } else {
          this.error = {
            message: 'No existe el usuario. Verifique sus datos!!!'
          }
          this.counter();
        }
      });
    });
  }

  signOut(): void {
    this.authService.signOut();
  }

  isAdmin() {
    return this.username === 'admin';
  }

  forgotPasswordSelected() {
    this.forgotPassword = {};
  }

  cancelForgotPassword() {
    this.forgotPassword = undefined;
  }

  singup() {
    console.log(this.newUser);
    let body = {
      newUser: this.newUser
    };
    this.User.create(body).subscribe((data: any) => {
      if (data.error) {
        // mostrar mensaje
        alert(data.message);
        return false;
      } else {
        alert(data.message);
        this.newUser = {
          gender: true
        };
        this.modalRef.hide();
        this.Router.navigateByUrl('reservation');
      }
    });
  }

  counter() {
    setTimeout(() => {
      this.error = undefined;
      this.logining = false;
    }, 5000);
  }

  selectGender(gender) {
    this.newUser.gender = gender;
  }

  toReservation() {
    this.modalRef.hide();
    this.Router.navigateByUrl('reservation');
  }

  sendConfirmation() {
    this.User.forgotPassword(this.forgotPassword.email).subscribe((data: any) => {
      console.log(data);
      setTimeout(() => {
        this.modalRef.hide();
      }, 5000);
    });
  }

  ngOnInit() {
    this.authService.authState.subscribe((user) => {
      this.user = user;
      this.loggedIn = (user != null);
      console.log(user);
    });
  }

  // validaciones

  validateField(field: string, f?: any) {
    this.fieldToValidate = field;
    console.log(f);
    if (this.newUser[field]) {
      if (f) {
        if (f.form.controls.email.status === 'VALID') {
          this.searching = true;
          if (field === 'email') {
            this.email_exist = true;
          }
          this.User.getByParam(this.newUser[field]).subscribe((data: any) => {
            console.log(data);
            this.fieldToValidate = undefined;
            if (field === 'username') {
              this.error_username = data.length > 0;
            } else {
              this.error_email = data.length > 0;
              if (data.verifier) {
                this.email_exist = JSON.parse(data.verifier.smtpCheck);
              }
              this.searching = false;
            }
          });
        }
      } else {
        this.User.getByParam(this.newUser[field]).subscribe((data: any) => {
          this.fieldToValidate = undefined;
          this.error_username = data.length > 0;
        });
      }
    } 
  }

  invalidForm(f: any) {
    return f.invalid || !this.newUser.gender || this.error_email || this.error_username;
  }

  validateNumbers(e: any) {
    var a = [];
    var k = e.which;

    for (let i = 48; i < 58; i++)
        a.push(i);

    if (!(a.indexOf(k)>=0))
        e.preventDefault();
  }

}
