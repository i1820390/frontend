import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { CategoriesService } from '../services/categories.service';
import { ProductsService } from '../services/products.service';
import { UsersService } from '../services/users.service';
import { Router } from '@angular/router';
import { TablesService } from '../services/tables.service';
import { PhonesService } from '../services/phones.service';
import { ModalDirective } from 'ngx-bootstrap';

@Component({
  selector: 'app-maintenance',
  templateUrl: './maintenance.component.html',
  styleUrls: ['./maintenance.component.css']
})
export class MaintenanceComponent implements OnInit {
  @ViewChild('newSaleModal', {static: true}) newSaleModal: ModalDirective;

  newProduct: any = {};
  newTable: any = {};
  newUser: any = {};
  categories: any = [];
  products: any = [];
  users: any = [];
  tables: any = [];
  userLoged: any;

  editingTable: boolean = false;
  editingUser: boolean = false;
  editingProduct: boolean = false;
  constructor(
    private Router: Router,
    private Categories: CategoriesService,
    private Products: ProductsService,
    private Phones: PhonesService,
    private Tables: TablesService,
    private el: ElementRef,
    private User: UsersService
  ) { 
    this.userLoged = JSON.parse(localStorage.getItem('userReservation'));
    localStorage.setItem('userReservation', JSON.stringify(this.userLoged));
    this.getCategories()
    this.getProducts();
    this.getUsers();
    this.getTables();
    this.getItemSelected();
  }

  reservations: any[] = [
    {
      id: 1,
      table_id: 1,
      products: [1,2,3],
      status: 'R',
      total_amount: Math.random()*30
    },
    {
      id: 2,
      table_id: 2,
      products: [1],
      status: 'A',
      total_amount: Math.random()*30
    },
    {
      id: 3,
      table_id: 3,
      products: [1,3],
      status: 'X',
      total_amount: Math.random()*30
    },
    {
      id: 4,
      table_id: 4,
      products: [2,3],
      status: 'R',
      total_amount: Math.random()*30
    }
  ];

  menu: any[] = [
    {
      id: 'reservation',
      name: 'reservas',
      selected: true
    },
    {
      id: 'management',
      name: 'administración',
      selected: false
    },
  ];

  itemSelected: any;

  addSale() {
    this.newSaleModal.config.ignoreBackdropClick = true;
    this.newSaleModal.show();
  }

  selectItem(m) {
    this.menu.map(i => i.selected = false);
    m.selected = true;
    this.getItemSelected();
  }

  getItemSelected() {
    this.itemSelected = this.menu.find(i => i.selected);
  }

  logout() {
    this.Router.navigateByUrl('logout');
  }

  getUsers() {
    this.User.findAllInexact().subscribe((data: any) => {
      this.users = data;
      this.getPhones();
    });
  }

  getPhones() {
    this.users.map(i => {
      i.password = undefined;
      this.Phones.findByUser(i.id).subscribe((data: any) => {
        if (data.length) {
          i.phone = data[0].phone_number;
        } else {
          i.phone = undefined;
        }
      });
    })
  }

  getTables() {
    this.Tables.findAll().subscribe((data: any) => {
      this.tables = data;
    });
  }

  getProducts() {
    this.Products.findAll().subscribe((data: any) => {
      this.products = data;
    });
  }

  getCategories() {
    this.Categories.findAll().subscribe((data: any) => {
      this.categories = data;
    });
  }

  saveProduct() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('photo', inputEl.files.item(0));
      this.Products.uploadPhoto(formData).subscribe((data: any) => {
        this.newProduct.photo = data.file.filename;
        this.Products.create(this.newProduct).subscribe((productCreated: any) => {
          this.newProduct = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getProducts();
          alert('Se registro el producto satisfactoriamente.');
        });
      });
    }
  }

  genders: any[] = [
    {
      id: 'male',
      title: 'masculino'
    },
    {
      id: 'female',
      title: 'femenino'
    }
  ];

  userTypes: any[] = [
    {
      id: 'fa fa-cog',
      title: 'Administrador',
      name: 'admin',
      selected: false
    },
    {
      id: 'fa fa-user',
      title: 'Usuario',
      name: 'user',
      selected: false
    },
    {
      id: 'fa fa-money',
      title: 'Cliente',
      name: 'customer',
      selected: false
    }
  ];

  searching: boolean = false;
  error_email: boolean = false;
  error_username: boolean = false;
  email_exist: boolean = true;

  selectGender(gender) {
    this.newUser.gender = gender;
  }

  invalidForm(f: any) {
    return f.invalid || !this.newUser.gender || this.error_email || this.error_username;
  }

  validateField(field: string) {
    if (this.newUser[field]) {
      this.searching = true;
      this.email_exist = true;
      this.User.getByParam(this.newUser[field]).subscribe((data: any) => {
        console.log(data);
        if (field === 'username') {
          this.error_username = data.length > 0;
        } else {
          this.error_email = data.result.length > 0;
          this.email_exist = JSON.parse(data.verifier.smtpCheck);
          this.searching = false;
        }
      });
    }
  }

  selectUserType(u: any) {
    this.userTypes.map(i => i.selected = false);
    u.selected = true;
    this.newUser.user_type = u.name;
  }

  getIcon(u: any) {
    let userFound = this.userTypes.find(i => i.name === u.user_type);
    if (userFound) {
      return userFound.id;
    } else {
      'NO SE REGISTRO TIPO';
    }
  }

  saveTable() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#tablePhoto');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('tablePhoto', inputEl.files.item(0));
      this.Tables.uploadPhoto(formData).subscribe((data: any) => {
        console.log(data);
        this.newTable.photo = data.file.filename;
        let body = {
          jsonbulk: [this.newTable]
        };
        this.Tables.create(body).subscribe((productCreated: any) => {
          this.newTable = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getTables();
          alert('Se registro el producto satisfactoriamente.');
        });
      });
    }
  }

  removeTable(id: number, index: number) {
    this.Tables.delete(id).subscribe((data: any) => {
      this.tables.splice(index, 1);
    });
  }

  removeProduct(id: number, index: number) {
    this.Products.delete(id).subscribe((data: any) => {
      this.products.splice(index, 1);
    });
  }

  removeUser(id: number, index: number) {
    this.User.delete(id).subscribe((data: any) => {
      this.users.splice(index, 1);
    });
  }

  editTable(table: any) {
    this.newTable = JSON.parse(JSON.stringify(table));
    this.editingTable = true;
  }

  saveEditTable() {
    console.log(this.newTable);
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#tablePhoto');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('tablePhoto', inputEl.files.item(0));
      this.Tables.uploadPhoto(formData).subscribe((data: any) => {
        console.log(data);
        this.newTable.photo = data.file.filename;
        let body = {
          jsonbulk: [this.newTable]
        };
        this.Tables.update(body).subscribe((productUpdated: any) => {
          console.log(productUpdated);
          this.newTable = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getTables();
          alert('Se registro el producto satisfactoriamente.');
        });
      });
    }
  }

  cancelEditTable() {
    this.newTable = {};
    this.editingTable = false;
  }

  cancelEditUser() {
    this.newUser = {
      gender: true
    };
    this.editingUser = false;
    this.userTypes.map(i => {
      i.selected = false;
    });
  }

  saveEditUser() {
    console.log(this.newUser);
    let body = {
      newUser: this.newUser
    };
    if (this.newUser.password) {
      this.User.changePassword(this.newUser.id, this.newUser.password).subscribe((data: any) => {
        console.log(data);
        console.log('Se cambio la contraseña');
      });
    }
    this.User.put(body).subscribe((data: any) => {
      console.log(data);
      this.newUser = {
        gender: true
      };
      this.editingUser = false;
      this.userTypes.map(i => {
        i.selected = false;
      });
      alert('Usuario Actualizado');
      this.getUsers();
    });
  }

  editUser(u: any) {
    this.newUser = JSON.parse(JSON.stringify(u));
    this.editingUser = true;
    this.userTypes.map(i => {
      i.selected = false;
    });
    let userTypeSelected = this.userTypes.find(i => i.name === this.newUser.user_type);
    if (userTypeSelected) {
      userTypeSelected.selected = true;
    }
  }

  editProduct(p: any) {
    this.newProduct = JSON.parse(JSON.stringify(p));
    this.editingProduct = true;
  }

  cancelEditProduct() {
    this.newProduct = {};
    this.editingProduct = false;
  }

  saveEditProduct() {
    let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
    let fileCount: number = inputEl.files.length;
    let formData = new FormData();
    if (fileCount > 0) { // a file was selected
      formData.append('photo', inputEl.files.item(0));
      this.Products.uploadPhoto(formData).subscribe((data: any) => {
        this.editingProduct = false;
        this.newProduct.photo = data.file.filename;
        this.Products.put(this.newProduct).subscribe((productCreated: any) => {
          this.newProduct = {};
          inputEl.files = undefined;
          inputEl.value = '';
          this.getProducts();
          alert('Se actualizo el producto satisfactoriamente.');
        });
      });
    }
  }


  ngOnInit() {
  }

}
