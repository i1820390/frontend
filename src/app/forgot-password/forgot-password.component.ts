import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { UsersService } from '../services/users.service';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  newPassword: any = {};
  userId: number;
  constructor(
    private ActivatedRoute: ActivatedRoute,
    private Router: Router,
    private User: UsersService
  ) { 

    this.ActivatedRoute.queryParams.subscribe((params) => {
      console.log(params);
      this.userId = parseInt(params.userId);
    });
  }

  changePassword() {
    this.User.changePassword(this.userId, this.newPassword.password).subscribe((data: any) => {
      alert('Se cambio tu contraseña.');
      this.Router.navigateByUrl('welcome');
    });
  }

  ngOnInit() {
  }

}
