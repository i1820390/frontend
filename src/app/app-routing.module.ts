import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WelcomeComponent } from './welcome/welcome.component';
import { ReservationComponent } from './reservation/reservation.component';
import { LogoutComponent } from './logout/logout.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ManagementReservationComponent } from './management-reservation/management-reservation.component';
import { MaintenanceComponent } from './maintenance/maintenance.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { AdminComponent } from './admin/admin.component';
import { ProductAdminComponent } from './admin/product-admin/product-admin.component';
import { ReservationAdminComponent } from './admin/reservation-admin/reservation-admin.component';
import { TableAdminComponent } from './admin/table-admin/table-admin.component';
import { UserAdminComponent } from './admin/user-admin/user-admin.component';
import { ReportsAdminComponent } from './admin/reports-admin/reports-admin.component';


const routes: Routes = [
  {
    path: '',
    component: WelcomeComponent
  },
  {
    path: 'welcome',
    component: WelcomeComponent
  },
  {
    path: 'reservation',
    component: ReservationComponent
  },
  {
    path: 'logout',
    component: LogoutComponent
  },
  {
    path: 'forgot-password',
    component: ForgotPasswordComponent
  },
  {
    path: 'management-reservation',
    component: ManagementReservationComponent
  },
  {
    path: 'maintenance',
    component: MaintenanceComponent
  },
  {
    path: 'about-us',
    component: AboutUsComponent
  },
  {
    path: 'contact-us',
    component: ContactUsComponent
  },
  {
    path: 'admin', component: AdminComponent,
    children: [
      {path: 'product', component: ProductAdminComponent},
      {path: 'reservation', component: ReservationAdminComponent},
      {path: 'table', component: TableAdminComponent},
      {path: 'user', component: UserAdminComponent},
      {path: 'reports', component: ReportsAdminComponent},
      {path: '', redirectTo: 'reservation', pathMatch: 'full'},
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
