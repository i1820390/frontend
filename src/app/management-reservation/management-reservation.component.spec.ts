import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ManagementReservationComponent } from './management-reservation.component';

describe('ManagementReservationComponent', () => {
  let component: ManagementReservationComponent;
  let fixture: ComponentFixture<ManagementReservationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ManagementReservationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ManagementReservationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
